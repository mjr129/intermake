====================================================================================================
                                      Intermake Documentation                                       
====================================================================================================

* `Overview<readme.rst>`_

* Using an `Intermake`:t: application:
    * `Basics<using/basic.rst>`_
    * `System requirements<using/requirements.rst>`_
    * `Advanced<using/advanced.rst>`_
    * `Troubleshooting<using/troubleshooting.rst>`_

* Creating an `Intermake`:t: application: 
    * `Basics<creating/basic.rst>`_
    * `Advanced<creating/advanced.rst>`_
    
* Developing `Intermake`:t:: 
    * `Basics<developing/basic.rst>`_
