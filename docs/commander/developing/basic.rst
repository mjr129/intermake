====================================================================================================
Development of Intermake itself
====================================================================================================

* `intermake`
    * The core library
* `intermake_qt`
    * Defines the GUI `intermake.Controller`. This is done separately so intermake will still
      run in CLI mode without Qt installed.
* `intermake_debug`, `intermake_tests`
    * Define sets of debug and test commands. `intermake_tests` also contains the main test suite. 

The source code itself is heavily documented inline.

To run the test cases::

    python -m intermake_test run_tests
    
    
To test a simple Intermake application:

    python -m intermake
    
    
To run an application with advanced debugging commands enabled:

    python -m intermake debug


Advanced settings are also available via:

    python -m intermake configure
