====================================================================================================
Usage tutorial
====================================================================================================

Where `sample` is the name of your application, the following command will launch it:

  ======================================= ==================================================
   Mode                                    Command                                          
  ======================================= ==================================================
   ARG: Command-line arguments             ``sample <commands>``                            
   CLI: Command line interface             ``sample``                                         
   GUI: Graphical user interface           ``sample gui``                                     
   PYI: Python interactive shell           ``sample pyi``                                     
   PYS: Interface for Python scripting     ``import sample``                                  
  ======================================= ==================================================

Intermake provides an ***extensive, context-dependent, in-line help*** system.

For instance, inside the `sample` CLI, use the `help` command to get started::


    $   help
        ECO help
        INF   _help____________________________________________
              Aliases: basic_help
        
                            You are in command-line mode.
        
    ... ... ... ... ... ... ... ... ... ... ... ... ... ... ...

