====================================================================================================
                                        System requirements                                         
====================================================================================================

* Iɴᴛᴇʀᴍᴀᴋᴇ uses type annotations, which require at least Pʏᴛʜᴏɴ 3.6.
    * This must be installed first. 

* Supported platforms
    * Windows 7,8
        * Iɴᴛᴇʀᴍᴀᴋᴇ uses Cᴏʟᴏʀᴀᴍᴀ to add support for ANSI-escape sequences
        * Iɴᴛᴇʀᴍᴀᴋᴇ instructs the user on how to enable UTF-8
    * Windows 10
    * Ubuntu
    * MacOS
        * Beware that this ships with a legacy version of Pʏᴛʜᴏɴ 2 by default, you'll need to update!

* Terminal environment requisites (`CLI`/`ARG`/`PYI`/`PYS`)
    * ANSI escape sequences
        * *If not supported*: Weird characters in console output
    * ANSI colours (optional)
        * *If not supported*: No colours will be shown
    * UTF8 (optional)
        * *If not supported*: Weird/missing data in console output.
            * Note: Pʏᴛʜᴏɴ itself must still be notified via the `PYTHONIOENCODING` environment variable, e.g. `export PYTHONIOENCODING=ascii:replace`. If this is not done the application will crash (Iɴᴛᴇʀᴍᴀᴋᴇ supplements the Python error with a more helpful error description).
    * *readline*
        * *If not supported*: Up/down will not work to invoke command history in console. 

* Graphical user interface requisites (`GUI`)
    * *PyQt5*.
        * Note: at the time of writing, some versions of Ubuntu ship with a broken *Qt*/*PyQt5*/*Sip* install, giving a `killed 9` or `segfault` error on GUI startup. This will require re-installation of *Qt*/*PyQt*/*Sip* by the user.
        * *If not supported*: Cannot start GUI. Iɴᴛᴇʀᴍᴀᴋᴇ's UI components are isolated so the console modes should still work fine providing the application itself also isolates its GUI (which it should).
    