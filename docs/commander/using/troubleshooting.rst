====================================================================================================
                              Troubleshooting an Intermake application                              
====================================================================================================

.. attention::

    * Please see |app_name|'s troubleshooting guide for specific issues

.. contents:: Table of Contents

----------------------------------------------------------------------------------------------------
                                      Starting the application                                      
----------------------------------------------------------------------------------------------------

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Application not found                              
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you get a message like |app_name| ``not found`` when trying to start |app_name| then Python
probably doesn't have its ``PATH`` configured correctly.
You might be able to start |app_name| using ``python -m`` |app_name|, but it's probably best to
consult the Python documentation at this time and fix the issue properly.

You probably need to add the Python binaries to your path:

+++++++
Windows
+++++++

You can modify your system ``PATH`` by searching for ``Environment Variables`` from the Start Menu. 

++++
Unix
++++

You can modify your system ``PATH`` by adding something like this to your ``.bash_profile``::

    export PATH=$PATH:/opt/local/Library/Frameworks/Python.framework/Versions/3.6/bin

Check out `this StackOverflow post`_ as a starting point.

.. _this StackOverflow post: https://stackoverflow.com/questions/35898734/pip-installs-packages-successfully-but-executables-not-found-from-command-line?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa



~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Syntax or Python errors
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

User errors:

* |app_name| requires at least Python 3.6.
    * (This is especially problematic on Mac, which ships with a legacy version of Python 2) 


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Unicode errors
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

User errors:

* You're using Python 2, |app_name| requires at least Python 3.6.
* You've changed the terminal encoding.
    * Check the solutions for Ubuntu and Windows below, regardless of your platform.

On Ubuntu:

* Problem: The terminal is using an implicit encoding `LC_ALL=C`. Python can't handle this.
    * Solution - Use UTF8. Call `export LC_ALL="en_GB.utf8"` from the command line.
        * (Replace the `en_GB` (UK) with your own locale, e.g. `es.utf8` for Spain or `zh_CN.utf8` for China.)

On Windows:

* Problem: `cmd.exe` or PowerShell is being used with an `ASCII`-only font.
    * Solution - Change your font to a Unicode one.
    * Quick workaround - call `set PYTHONIOENCODING=ascii:replace` from the command line
    
On Mac:

* No known problems unless you've reconfigured your terminal, see "User errors" above.


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
General failure to start
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

User errors:

* Problem: Requisite libraries not installed
    * Solution: Please follow the installation instructions included with the application, generally your software should have been installed via `pip`, here are some common errors:
        * You are using `pip` for Python 2 to try and install a Python 3 application.
        * You are running `pip` without sufficient privileges:
            * Use `sudo` (UNIX)
            * Use the administrator shell (Windows)
            * Use `virtualenv`

Coding errors:

* Problem: A Iɴᴛᴇʀᴍᴀᴋᴇ error occurs.
    * Solution: Report it so it can be fixed. Please include the debug information in your bug report. 
        * GUI: Iɴᴛᴇʀᴍᴀᴋᴇ writes debugging information to the console (`stderr`) whenever it encounters an error in the GUI.
          Launch the GUI from the command line (e.g. `./xxxx "ui gui"`) to see this information.
        * CLI: If an error occurs in the CLI version, details can be retrieved using the `error` command.
            * You can instruct all errors to be dumped by default via the `set` command.
    * Solution: Fix it yourself! All Iɴᴛᴇʀᴍᴀᴋᴇ code is heavily documented in-line using standard Pʏᴛʜᴏɴ documentation. 


----------------------------------------------------------------------------------------------------
                                           General issues                                           
----------------------------------------------------------------------------------------------------

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Obtaining binaries                                         
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Binaries are not usally available for Python applications, installation `using Pip`_ is the
recommended method.
For deployment on systems without an internet connection, you can create your own binaries using
PyInstaller_:

.. parsed-literal::

    $   cd |app_name|
    $   pyinstaller groot/__main__.py
    
    
.. _using Pip: installation.md
.. _PyInstaller: https://www.pyinstaller.org/


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
My extensions are not showing
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Follow the instructions given in |app_name|'s "extending" guide
* Remember to install your extension package using ``pip install -e .``
* Remember to register your extension package with |app_name| using |app_name| ``import my_library_name +persist``.
* Remember the ``@`` when adding the decorator to your extension function.


----------------------------------------------------------------------------------------------------
                                   Command line interface errors                                    
----------------------------------------------------------------------------------------------------

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Hard to read text in CLI mode
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|app_name| does its best to use colours to distinguish on-screen items, and expects standard
ANSI/DOS colours.

If you're getting unreadable items, such as yellow text on a white background, the colours should be
changed in your terminal preferences (not the Intermake application itself).
Modify your terminal profile/theme or simply turn off ANSI colour support.


----------------------------------------------------------------------------------------------------
                                  Graphical user interface errors                                   
----------------------------------------------------------------------------------------------------

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Segmentation fault, killed 9, or GUI fails to run
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On Ubuntu:

* Problem: At the time of writing, some Linux systems have a corrupt installation of PyQt and/or Qt.
    * Solution: Build PyQt and/or Qt properly from source yourself, see: 
        * https://riverbankcomputing.com/software/pyqt/intro
        * https://www.qt.io/
    * Workaround: Just use |app_name| from one of the CLI modes.
    
On Windows or Mac:

* No known problems

On other systems (e.g. Android):

* Problem: Qt is not supported.
    * Solution: See if you can find a Qt installation for your system.
    * Workaround: Use |app_name| from one of the CLI modes.


.. |app_name| replace:: “app_name”
