========================================================================================================================
                                                     Advanced usage                                                     
========================================================================================================================

------------------------------------------------------------------------------------------------------------------------
                                               Changing the main settings                                               
------------------------------------------------------------------------------------------------------------------------


    
.. note::

    * GUI: This command is an *advanced* command and is hidden by default. Please see the `listing special commands`_
      section below for information on how to enable advanced commands.




------------------------------------------------------------------------------------------------------------------------
                                                Listing special commands                                                
------------------------------------------------------------------------------------------------------------------------

Some commands may be hidden by default.
To show them you can use the `use` command.

**CLI**::

    use advanced
    
**Python**::

    intermake.toggle_command_set( "advanced" )
    
.. note::

    * GUI: From the GUI you will be unable to use the advanced commands without showing them first.
    * Other modes: These modes still allow you to run commands regardless of whether they are hidden or not, however
      typing `cmdlist` will now show the `advanced` commands.


------------------------------------------------------------------------------------------------------------------------
                                          Configuring the workspace directory                                           
------------------------------------------------------------------------------------------------------------------------

`Intermake`:t: stores its data in a "workspace" this folder is by default `~/.intermake-data/application-name`, however
you can change this by using the `workspace` command.

**CLI**::
    
    workspace c:\my\folder
    
**Python**::

    intermake.change_workspace( "workspace" )
        
.. note::

    * GUI: This command is an *advanced* command and is hidden by default. Please see the `listing special commands`_
      section for information on how to enable advanced commands.


------------------------------------------------------------------------------------------------------------------------
                                              Switching the user interface                                              
------------------------------------------------------------------------------------------------------------------------

Use the `ui` command.

**CLI**::
    
    ui gui
