====================================
Further details: Intermake Commander
====================================

Please see the `main page`_ for the description of this package.

.. _`main page`: ../readme.rst


----------------------------------------------------------------------------------------------------
                                           Sample code                                              
----------------------------------------------------------------------------------------------------

example/__init__.py::

    from intermake import Application 

    app = Application( "Example" )

    @app.command()
    def say_hello(n : int):
        """says hello n times"""
        print("hello " * n)

example/__main__.py::

    import example
    example.app.start()

Usage::

      example say_hello 2
    ⇝ hello hello
    
      example help say_hello
    ⇝ says hello n times

      example gui
    ⇝ welcome to Example's graphical user interface
      *click say hello*
      *set n = 2*
    ⇝ *message box: hello hello*
      *click x*
        
      example pyi
    ⇝ welcome to Example's python interactive shell
      say_hello(2)
    ⇝ hello hello
      exit()

      example cli
    ⇝ welcome to Example's command line interactive
      say_hello 2
    ⇝ hello hello
      exit

      python
      import example
      example.say_hello(2)
    ⇝ hello hello
      exit()

----------------------------------------------------------------------------------------------------
                                           Example usage                                            
----------------------------------------------------------------------------------------------------

* [Bio42](https://bitbucket.org/mjr129/bio42)
* [Groot](https://bitbucket.org/mjr129/groot)
