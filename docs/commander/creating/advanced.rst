====================================================================================================
Developing advanced applications with Intermake
====================================================================================================

.. contents::

----------------------------------------------------------------------------------------------------
Documenting commands and parameters
----------------------------------------------------------------------------------------------------

When you create an Intermake `Command` using `@command` or `@my_app.command`, Intermake obtains:

* Command documentation
    * from the PEP-287 docstrings on the function
* Command parameter documentation
    * from the `:param xxx:` role in documentation of the function containing the parameter
* Options (for parameters that are `Enum`s or `Flags`)
    * from iterating the `Enum` or `Flags` class
* `Enum` option documentation
    * from the `:cvar xxx:` role in the documentation of the `Enum` class containing the option

The default Intermake `SettingsCommand` class also obtains:

* Documentation for individual fields
    * From the `:ivar xxx` role in the documentation of the class containing the field


----------------------------------------------------------------------------------------------------
Calling commands internally
----------------------------------------------------------------------------------------------------

Sometimes, you may wish to invoke a function as if the *user* had launched it. For instance you
might create a user-interface with a button that launches a command with a predefined parameter
set.

`@command` normally creates a `Command` object from the decorated function and registers that
object with the most recently instantiated `Application`. The function itself is not modified.

The `Command` object associated with a function ``fn`` may be retrieved using::

    import intermake
    intermake.BasicCommand.retrieve( fn )

A `Command`, `cmd`, may be invoked as if the user had invoked it using::

    import intermake
    intermake.acquire( cmd ).run()
    
* Pass function arguments into `run`.
* Pass UI hints, such as which window to show the result in, into `acquire`
* `acquire` accepts either a `Command` instance, or a function with an associated `Command` 


. note::

    Note that the default `Controller`s are intelligent enough to work when you are calling a
    command from within the execution chain of *another* command. In these cases they will retain
    the existing worker thread and not, for instance, start a new worker thread and window. This
    means `acquire` can be used in cases where the calling thread is indeterminate. 

----------------------------------------------------------------------------------------------------
Creating your own frontend or GUI
----------------------------------------------------------------------------------------------------

You can provide your own frontend by subclassing `Controller`.
`Application` will also need to be subclassed or monkey-patched, and `on_create_ui_controller`
overridden to yield an instance of your own `Controller`.

An commented example of this can be found in the Groot_ application.

.. _Groot: https://bitbucket.org/mjr129/groot


----------------------------------------------------------------------------------------------------
Adding support for new types
----------------------------------------------------------------------------------------------------

If you have a custom type, lets say a `DateTime` class, you probably want Intermake to recognise
it, for instance by providing a calendar in the GUI and allowing the user to type dates like
`xx/xx/xx` in the console.


+++
GUI
+++

* In GUI mode, Intermake uses the `Editorium`:t library to supply the Qt GUI editor.
    * To add GUI support for new types call `.editorium.register` on the `GuiController`.  
    * In the `on_create_ui_controller` method of your `Application` is an ideal place to do this.
    * See the Editorium_ package itself for details.

.. _Editorium: https://bitbucket.org/mjr129/editorium

+++
CLI
+++

* Intermake uses the `StringCoercion`:t: library to convert from text in both the CLI and GUI.
    * To add support for new types call `.coercers.register` on the `Controller`
    * In the `on_create_ui_controller` method of your `Application` is an ideal place to do this.
    * See the StringCoercion_ package itself for details.
    
.. _StringCoercion: https://bitbucket.org/mjr129/stringcoercion

    
----------------------------------------------------------------------------------------------------
Creating advanced commands
----------------------------------------------------------------------------------------------------

The `@command` decorator creates a `Command` object and calls `register` on that created object.

If you'd like more control over the object created, you can subclass `Command` and provide
the functionality yourself, passing `register` an instance of your class yourself.

As an example, Intermake's `SetterCommand` provides a derivation of `Command` that has variable
arguments, depending on the settings object provided to it.


----------------------------------------------------------------------------------------------------
Creating extensions
----------------------------------------------------------------------------------------------------

Intermake applications can build on top of each other, providing increased levels of functionality.

* To derive one application from another.
    * Create a new Intermake application, but pass the `inherit` parameter to your `Application`
      instance, specifying the existing application instance.
    * Derived applications inherit commands, help and non-overridden parameters, such as the name
      and version, from their ancestors.

* To create an extension for an existing application:
    * Create your own package, using `@your_app.command` to register your own commands with
      ``your_app``, or `@command` to register your own commands with the currently running
      application.
    * Import your extension into ``your_app``, for instance using `import` from the CLI or Python.

