====================================================================================================
A tutorial on developing applications with Intermake
====================================================================================================

----------------------------------------------------------------------------------------------------
Implementation
----------------------------------------------------------------------------------------------------

Get right in there with the full implementation of a simple Intermake application, called
``eggsample``!
The Egg Sample has two amazing functions "`say_hello`" and "`do_some_work`".
Paste the following contents into the appropriate files:

``[eggsample/eggsample/__init__.py]``

.. code-block:: python                                                            
                                                                                                     
   import intermake as im                                   # Import intermake                          
                                                            #                                            
   my_app = im.Application( name = "sample" )               # Create our application           
                                                            #                                            
   @my_app.command()                                        # Add a command to our application...    
   def say_hello():                                         # ...define it                        
       """                                                  # ...document it    
       Says hello                                           # ..."                                         
       """                                                  # ..."                                         
       print( "hello" )                                     # ...make it do something             
                                                            #                                            
   @im.command()                                            # Add another command                       
   def do_some_work( count : int ):                         # ...define it                                         
       """                                                  # ...document it                                         
       Does nothing.                                        # ..."                                         
                                                            # ..."                                         
       :param count: The number of times to                 # ..."                                         
                     do nothing.                            # ..."                                         
       """                                                  # ..."                                         
       with im.pr.pr_action( "Working!", count ) as action: # ...show a progress bar in the current UI     
            for n in range( count ):                        # ..."                 
                action.increment()                          # ..."                   



``[eggsample/eggsample/__main__.py]``:

.. code-block:: python                           
                                                 
     
                                                            # 
    def main():                                             # Entry point
        import eggsample                                    #
        eggsample.my_app.start()                            # Start our application's UI
                                                            #
    if __name__ == "__main__":                              # Python boilerplate check
        main()                                              #


``[eggsample/setup.py]``:


 .. code-block::                                                                    
                                                                                    
    from distutils.core import setup                                                # Python boilerplate installer               
                                                                                    # "                                          
                                                                                    # "                                          
    setup( name = "eggsample",                                                      # "                                          
           packages = ["eggsample"],                                                # "                                          
           entry_points = { "eggsample": ["eggsample = eggsample.__main__:main"] }, # "                                          
           install_requires = ["intermake"] )                                       # "                                          


*That's all there is!*

----------------------------------------------------------------------------------------------------
Running our application
----------------------------------------------------------------------------------------------------

This is all Python standard stuff, but if you don't already know, do a development installation with
``pip``::

    cd eggsample
    sudo pip install -e .

Now from the command line, you should now be able to run::

    eggsample

You can try out the various modes as follows. They all do the same thing, in different ways.


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
CLI mode
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block::

    BASH   <<< eggsample
    SAMPLE <<< say_hello
           <<< do_some_work 10000
           <<< exit

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ARG mode
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block::

    BASH   <<< eggsample say_hello then do_some_work 10000


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
GUI mode
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block::

    BASH   <<< eggsample "ui gui"
    SAMPLE <<< *click say_hello*
           <<< *click run*
           <<< *click do_some_work*
           <<< *set count to 10000*
           <<< *click run*
           <<< *close the window*

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
PYI mode
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block::

    BASH   <<< eggsample "ui pyi"
    PYTHON <<< say_hello()
           <<< do_some_work(10000)

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
PYS mode
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block::

    PYTHON <<< import eggsample
           <<< eggsample.say_hello()
           <<< eggsample.do_some_work(10000)
