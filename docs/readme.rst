====================================================================================================
                                             Intermake                                              
====================================================================================================

Intermake is a set of tools used to automatically create application frontends.

Intermake aims to **avoid repetition**, pulling the required information from PEP484_ annotations
and doc-strings. However, it also aims to be **explicit**, favouring standard OOP classes and
avoiding behind the scenes magic and duck-typing where possible.

.. attention::

    **Full documentation can be found `here<http://software.rusilowicz.com/intermake>`_.**

.. contents::

-------------------
Intermake Libraries
-------------------

++++++++++++
IM Commander
++++++++++++

`IM Commander`:t: provides a container to which commands and/or help topics may be added.
These commands are then exposed through the user's choice of UI. Out of the box, IM provides a QT
GUI, a command line UI and a Python Interactive Session. ::

    #example/__init__.py::
    from intermake import Application 

    app = Application( "Example" )

    @app.command()
    def say_hello(n : int):
        """says hello n times"""
        print("hello " * n)
        
* More details available on the `Commander`_ page.


+++++++++++++++++++
IM String Editorium
+++++++++++++++++++

When the user types some text, such as on the command line, `String Editorium`:t: provides a way
of coercing that text to a concrete python object denoted by an *annotation*. The ``typing``
library is supported out of the box, as well as a suite of custom annotations, so the string
editorium supports coercing everything from simple `int`s to `Optional[Union[int, MyCustomClass]]`.

* More details available on the `String Editorium`_ page.

+++++++++++++++
IM Qt Editorium
+++++++++++++++

The Qt counterpart of the String Editorium. The `QT Editorium`:t: dynamically provides editors for
typed (annotated) fields, and is capable of generating full dialogues to fill out
function-parameters or multi-field custom classes.

* More details available on the `Qt Editorium`_ page.

+++++++++
SXS Relay
+++++++++

Sending output data to ``stdout`` only works if the command in question is being run from the
terminal. The `SXS Relay`:t: provides an alternative stream for abstractly formatted data, such as
tables and definitions. This stream can then be picked up by the UI and presented in a more useful
fashion. Out of the box SXS supports HTML and 24-bit coloured ANSI outputs.

* More details available on the `SXS Relay`_ page.





.. **** REFERENCES ****

.. _click: https://palletsprojects.com/p/click
.. _PEP484: https://www.python.org/dev/peps/pep-0484
.. _`Commander`: commander/readme.rst
.. _`String Editorium`: str_editorium.rst
.. _`Qt Editorium`: qt_editorium.rst
.. _`SXS Relay`: sxsxml.rst
