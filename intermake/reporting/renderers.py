from intermake.reporting.classes import Nested, Report, Item, Text, Table, Row, Cell, HeaderCell, UnorderedList, OrderedList, ListItem, Paragraph, Section, Heading, Span


class Renderer:
    def render( self, item: Item ):
        pass


class ItemRenderer:
    def enter( self ):
        pass
    
    
    def data( self ):
        pass
    
    
    def exit( self ):
        pass


class TextRenderer( Renderer ):
    mapping = { }
    
    
    def render( self, item: Item ):
        if isinstance( item, Text ):
            return item.text
        elif isinstance( item, Nested ):
            r = []
            start, end = self.mapping[type( item )]
            
            if callable( start ):
                start = start()
            
            r.append( start )
            
            for child in item:
                r.append( self.render( child ) )
            
            if callable( end ):
                end = end()
            
            r.append( end )
            
            return "".join( r )


class HtmlRenderer( TextRenderer ):
    
    
    def __init__( self ):
        self.section_depth = 0
    
    
    def enter_section( self ):
        self.section_depth += 1
        return "<section>"
    
    
    def exit_section( self ):
        self.section_depth -= 1
        return "</section>"
    
    
    def enter_heading( self ):
        return "<h{}>".format( self.section_depth )
    
    
    def exit_heading( self ):
        return "</h{}>".format( self.section_depth )
    
    
    mapping = \
        {
            Report       : ("<html><body>", "</body></html>"),
            Table        : ("<table>", "</table>"),
            Row          : ("<tr>", "</tr>"),
            Cell         : ("<td>", "</td>"),
            HeaderCell   : ("<th>", "</th>"),
            UnorderedList: ("<ul>", "</ul>"),
            OrderedList  : ("<ol>", "</ol>"),
            ListItem     : ("<li>", "</li>"),
            Paragraph    : ("<p>", "</p>"),
            Section      : (enter_section, exit_section),
            Heading      : (enter_heading, exit_heading),
            Span         : ("<span>", "</span>"),
        }

