class Item:
    pass


class Text( Item ):
    def __init__( self, text: str ):
        self.text = text


class Nested( Item ):
    def __init__( self, content = None ):
        self.__items = list( content ) if content is not None else []
    
    
    def __iter__( self ):
        return iter( self.__items )
    
    
    def add( self, *args, **kwargs ):
        v = args[0]
        
        if isinstance( v, Item ):
            item = v
        elif isinstance( v, str ):
            assert not args[1:]
            assert not kwargs
            item = Text( v )
        elif isinstance( v, type ) and issubclass( v, Item ):
            item = v( *args[1:], **kwargs )
        else:
            raise TypeError( "args[0]" )
        
        self.__items.append( item )
        return item


class Report( Nested ):
    pass


class Cell( Nested ):
    pass


class HeaderCell( Cell ):
    pass


class Row( Nested ):
    __item_type__ = Cell


class ListItem( Nested ):
    pass


class Table( Nested ):
    __item_type__ = Row
    
    
    def __init__( self ):
        super().__init__()


class Paragraph( Nested ):
    pass


class UnorderedList( Nested ):
    __item_type__ = ListItem


class OrderedList( Nested ):
    __item_type__ = ListItem


class Section( Nested ):
    pass


class Heading( Nested ):
    pass


class ESpan:
    code = "code"
    command = "command"
    file = "file"
    key = "key"
    value = "value"
    positive = "positive"
    negative = "negative"
    neutral = "neutral"
    error = "error"
    warning = "warning"
    verbose = "verbose"


class Span( Nested ):
    pass
