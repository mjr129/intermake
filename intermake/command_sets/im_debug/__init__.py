"""
Intermake debugging commands.

If this modules is loaded then the debug commands become available in all intermake `Application`s.
"""

from . import commands
