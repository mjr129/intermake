from mhelper import ArgInspector, Documentation, NOT_PROVIDED, array_helper, markdown_helper

from intermake.commander import BasicCommand, Command, Controller, HelpTopic, pr


def rst_to_sxs( doc: str ) -> str:
    """
    Formats markdown.
    
    :param doc:        Restructured text.
    :return:           Formatted text.
    """
    if doc is None:
        doc = ""
    
    doc = doc.strip()
    doc = markdown_helper.markdown_to_sxs( doc )
    return doc.strip()


def get_argument_help( argument_: ArgInspector ):
    r = []
    t = argument_.annotation.get_indirect_subclass( object )
    
    if t is None:
        raise ValueError( "Cannot obtain type above object from «{}».".format( argument_.annotation ) )
    
    r.append( "<table>" )
    r.append( "<tr><td>{}</td><td>{}</td></tr>".format( "name", argument_.name ) )
    r.append( "<tr><td>{}</td><td>{}</td></tr>".format( "type name", t.__name__ ) )
    r.append( "<tr><td>{}</td><td>{}</td></tr>".format( "optional", argument_.annotation.is_optional ) )
    r.append( "<tr><td>{}</td><td>{}</td></tr>".format( "default", argument_.default ) )
    r.append( "<tr><td>{}</td><td>{}</td></tr>".format( "description", argument_.description ) )
    r.append( "</table>" )
    
    # Type docs
    docs = Documentation( t.__doc__ )["cvar"]
    r.append( docs.get( "", "" ) )
    
    rows = ([["value", "comments"]] +
            [[key, value] for key, value in docs.items() if key and value])
    
    r.append( pr.fmt_table( rows ) )
    return "\n".join( r )


def get_topic_help( command_: HelpTopic ) -> str:
    r = []
    r.append( "<section name='{}'>".format( command_.name ) )
    x = command_.to_html()
    x = x.replace( "|app_name|", Controller.ACTIVE.app.name )
    r.append( x )
    r.append( "</section>" )
    return "\n".join( r )


def get_command_help( command: Command ) -> str:
    """
    Gets the help text of the specified `Command`, formatted for display in the CLI and returned as a single string.
    """
    result = []
    name: str = command.name
    
    result.append( "<section name='COMMAND: {}'>".format( name ) )
    
    result.append( "<verbose>Command line aliases: {}</verbose>".format( ", ".join( command.names ) ) )
    
    command_name = name
    
    if isinstance( command, BasicCommand ):
        command_name = command.function.__name__
        result.append( "<verbose>Python aliases      : {}</verbose>".format( command.function.__module__ + "." + command.function.__name__ ) )
    
    args = []
    
    for arg in command.args:
        n = arg.name
        
        if args:
            n = ", " + n
        
        if arg.default is NOT_PROVIDED:
            args.append( n )
        else:
            args.append( "[" + n + "]" )
    
    arg_text = "".join( args )
    
    if len( arg_text ) > 100:
        arg_text = "&lt;arguments&gt;"
    
    result.append( "<verbose>Command line usage  : {} {} {}</verbose>".format( Controller.ACTIVE.app.name, name, arg_text.replace( "[, ", " [" ).replace( ",", "" ) ) )
    result.append( "<verbose>Python usage        : {}( {} )</verbose>".format( command_name, arg_text ) )
    result.append( "" )
    
    #
    # DESCRIPTION
    #
    desc = command.documentation
    desc = rst_to_sxs( desc )
    result.append( desc )
    result.append( "" )
    
    #
    # ARGUMENTS
    #
    extra: str = None
    
    result.append( "<table>" )
    result.append( pr.fmt_row( ["argument", "type", "default", "description"] ) )
    
    for i, arg in enumerate( command.args ):
        arg: ArgInspector = arg
        desc = rst_to_sxs( arg.description )
        t = arg.annotation
        
        viable_subclass_type = array_helper.first_or_none( t.get_indirect_subclass( x ) for x in __iter_enumerative_types() )
        
        if viable_subclass_type is not None:
            desc += "‡"
            for k in viable_subclass_type.__dict__.keys():
                if not k.startswith( "_" ):
                    desc += "\n" + " * <value>{}</value>".format( k )
            
            if not extra:
                extra = arg.name
        
        # Arg type
        if t.is_optional:
            v = arg.annotation.optional_value
            t = "{}/none"
        else:
            v = t.value
            t = "{}"
        
        t = t.format( v.__name__.lower() if isinstance( v, type ) and len( v.__name__ ) < 10 else "obj" )
        
        # Default
        d = arg.default
        
        if isinstance( d, str ):
            d = '"{}"'.format( d )
        elif d is NOT_PROVIDED:
            d = "-"
        else:
            d = str( d )
        
        if len( d ) > 10:
            d = "..."
        
        result.append( pr.fmt_row( [pr.escape( arg.name ),
                                    pr.escape( t ),
                                    pr.escape( d ),
                                    desc],
                                   escape = False ) )
    
    result.append( "</table>" )
    
    if extra:
        result.append( "" )
        result.append( "<verbose>"
                       "‡ Specify the argument when you call <command>help</command> "
                       "to obtain the full details for these values. E.g. “"
                       "<command>help {} {}</command>”."
                       "</verbose>".format( command.name, extra ) )
        result.append( "" )
    
    result.append( "</section>" )
    return "\n".join( result )


# noinspection PyUnresolvedReferences
# noinspection PyPackageRequirements
def __iter_enumerative_types():
    try:
        from flags import Flags
        yield Flags
    except ImportError:
        pass
    
    try:
        from enum import Enum
        yield Enum
    except ImportError:
        pass
