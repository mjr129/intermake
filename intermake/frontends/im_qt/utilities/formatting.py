from mhelper import string_helper


def get_nice_name( command ):
    return string_helper.capitalise_first_and_fix( command )
