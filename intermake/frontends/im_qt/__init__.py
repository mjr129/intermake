"""
The API of Intermake's Qt interface.

Note: Resources are subject to change and are not exported. 
"""
from .forms.frm_big_text import FrmBigText
from .forms.frm_maintenance import FrmMaintenance
from .forms.frm_intermake_main import show_basic_window
from .utilities.interfaces import IGuiMainWindow
from .extensions.gui_controller import GuiController, GuiControllerWithBrowser
from .extensions import resources_extensions as __2
from .utilities.formatting import get_nice_name


__2.init()
