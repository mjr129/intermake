import warnings
from typing import List, Optional, Callable

from intermake.commander.engine import threaded_stream
from intermake.commander.engine.abstract_command import Command
from mhelper import MEnum, ArgsKwargs, ArgValueCollection, TracebackCollection, exception_helper


__author__ = "Martin Rusilowicz"


class _EResultState( MEnum ):
    PENDING = 0
    SUCCESS = 1
    FAILURE = 2


class ResultRelay:
    def __init__( self, output ):
        self.output = output
        self.history = []
    
    
    def write( self, message ):
        self.history.append( message )
        self.output.write( message )


class Result:
    """
    Intermake asynchronous result container and command executor.
    
    .. note::
    
        * Receiving:
            * All callbacks will be made in the primary thread, i.e. the
              same thread that requested the result.
        * Calling:
            * The `Result` class is not itself thread safe - it is up to
              the host to ensure that `set_result` or `set_error` is
              called in the main thread.
          
    :cvar __INDEX:          Tally on result count.  
    :ivar state:            State of result (pending, success, failure)
    :ivar ui_controller:             Executing host
    :ivar result:           Result (on success)
    :ivar exception:        Exception (on failure)
    :ivar traceback:        Exception traceback (on failure)
    :ivar messages:         Messages (on success or failure)
    :ivar index:            Index of result (arbitrary)
    :ivar __callbacks:      Result listeners (when pending)
    """
    __INDEX = 0
    
    __slots__ = "command", "args", "ui_args", "state", "ui_controller", "result", "exception", "traceback", "messages", "index", "__callbacks"
    
    
    def __init__( self,
                  *,
                  command: Command,
                  args: ArgsKwargs,
                  controller_args: ArgsKwargs,
                  controller ) -> None:
        """
        Constructs a `Result` in the `PENDING` state.
        """
        from intermake.commander.engine.abstract_controller import Controller
        assert isinstance( controller, Controller )
        
        Result.__INDEX += 1
        
        self.command: Command = command
        self.args: ArgsKwargs = args
        self.ui_args: ArgsKwargs = controller_args
        self.state = _EResultState.PENDING
        self.ui_controller: Controller = controller
        self.result: Optional[object] = None
        self.exception: Exception = None
        self.traceback: Optional[TracebackCollection] = None
        self.messages: Optional[List[str]] = None
        
        self.index = Result.__INDEX
        self.__callbacks: List[Callable[[Result], None]] = []
    
    
    def execute( self, output ) -> None:
        """
        Runs the command.
        
        * This action can be performed in *any thread*.
        * This method may raise any exception.
        * It is the *caller's* responsibility to ensure that `issue_callbacks`
          is called to call the callbacks.
        """
        if self.state != _EResultState.PENDING:
            raise ValueError( "Cannot execute a task that already has a result." )
        
        # Point the relay at us
        relay = ResultRelay( output )
        original = threaded_stream.SXS_RELAY.get_target()
        threaded_stream.SXS_RELAY.set_target( relay )
        
        
        
        try:
            # Check the argument types are correct
            ArgValueCollection( self.command.args, read = self.args )
            
            # Run the command
            r = self.command.on_run( *self.args.args, **self.args.kwargs )
            self.state = _EResultState.SUCCESS
            self.result = r
        except BaseException as ex:
            self.state = _EResultState.FAILURE
            self.exception = ex
            self.traceback = exception_helper.get_traceback_ex( ex )
        finally:
            self.messages = relay.history
            threaded_stream.SXS_RELAY.set_target( original )
    
    
    def cancel( self, message = None ):
        """
        Cancels the task before it has even begun.
        :param message:     Optional message.
        :return:            Nothing. 
        """
        from intermake.commander.engine.abstract_controller import TaskCancelledError
        
        if self.state != _EResultState.PENDING:
            raise ValueError( "Cannot cancel a task that already has a result." )
        
        message = message or "This task was cancelled before it was initiated."
        self.state = _EResultState.FAILURE
        self.exception = TaskCancelledError( message )
        self.traceback = None
        self.messages = [message]
    
    
    @property
    def title( self ):
        """
        The title of this Result.
        """
        warnings.warn( "Deprecated - use `command.name`", DeprecationWarning )
        return self.command.name
    
    
    def listen( self, callback: Callable[["Result"], None] ):
        """
        Adds a callback.
        
        This function calls `callback` if the result has completed, otherwise
        `callback` is called when the result completes.
        """
        if self.is_pending:
            self.__callbacks.append( callback )
        else:
            callback( self )
    
    
    def issue_callbacks( self ):
        """
        Calls the callbacks, including the host itself.
        """
        
        # Call any global callbacks
        self.ui_controller.app.on_executed( self )
        self.ui_controller.result_history.append( self )
        self.ui_controller.on_executed( self )
        
        for callback in self.__callbacks:
            callback( self )
        
        self.__callbacks = None
    
    
    def raise_exception( self ) -> None:
        """
        For a result in the `FAILURE` state, re-raises the exception, otherwise does nothing.
        """
        if self.exception:
            raise self.exception
    
    
    @property
    def is_success( self ) -> bool:
        """
        `True` if the `Result` is in the `SUCCESS` state.
        """
        return self.state == _EResultState.SUCCESS
    
    
    @property
    def is_error( self ) -> bool:
        """
        `True` if the `Result` is in the `ERROR` state.
        """
        return self.state == _EResultState.FAILURE
    
    
    @property
    def is_pending( self ) -> bool:
        """
        `True` if the `Result` is in the `PENDING` state.
        """
        return self.state == _EResultState.PENDING
    
    
    def __repr__( self ):
        """
        Imperfect Python representation.
        """
        if self.is_pending:
            return "{}({}, {}, {}, {})".format( type( self ).__name__, self.index, self.command, repr( self.args ), repr( self.state ) )
        elif self.is_success:
            return "{}({}, {}, {}, {}, result = {})".format( type( self ).__name__, self.index, self.command, repr( self.args ), repr( self.state ), repr( self.result ) )
        else:
            return "{}({}, {}, {}, {}, exception = {})".format( type( self ).__name__, self.index, self.command, repr( self.args ), repr( self.state ), repr( self.exception ) )
    
    
    def __str__( self ) -> str:
        """
        Complete string representation.
        """
        if self.is_pending:
            return "(Pending) {} {}".format( self.command, self.args )
        elif self.is_success:
            return "(Success) {} {} = {}".format( self.command, self.args, self.result )
        else:
            return "(Failure) {} {} = {}".format( self.command, self.args, self.exception )
