"""
Defines the console host and support classes.
"""
import sys

from mhelper import ArgInspector, SwitchError, io_helper

import builtins
from intermake.commander.engine import Controller, EImRunMode, Result, SxsToStderrWriter
from intermake.commander.framework import console_configuration


class ConsoleController( Controller ):
    """
    UI controller for all the default console-based modes.
    """
    
    
    def __init__( self, app, mode: str ):
        """
        CONSTRUCTOR
        See the `Core.initialise` function for parameter descriptions.
        """
        super().__init__( app, mode )
    
    
    def get_prompt( self ) -> str:
        """
        Obtains the prompt used in the CLI.
        """
        return self.on_get_prompt()
    
    
    def on_get_prompt( self ) -> str:
        """
        VIRTUAL OVERRIDE
        
        `ConsoleController` provides an overridable `on_get_prompt` that may be
        used to provide custom prompts in the CLI.
        """
        return "{}>".format( self.app.name.lower() )
    
    
    def on_stop( self ) -> bool:
        if self.mode == EImRunMode.JUP:
            print( "The Jupyter frontend cannot be stopped without exiting the application. The application will now exit." )
            return False
        
        return True
    
    
    def __str__( self ) -> str:
        return "{}('{}' in '{}' mode)".format( type( self ).__name__, self.app.name, EImRunMode.get_name( self.mode ) )
    
    
    def on_start( self ) -> bool:
        """
        OVERRIDE 
        """
        if self.mode not in (EImRunMode.PYS, EImRunMode.ARG):
            if console_configuration.current.welcome_message:
                print( "{} {} {}. Type 'help{}' for help.".format( self.app.name.upper(), self.app.version, EImRunMode.get_name( self.mode ), "()" if self.mode != EImRunMode.CLI else "" ) )
        
        if self.mode in (EImRunMode.ARG, EImRunMode.CLI, EImRunMode.IPYI):
            from intermake.commander.framework.console_parser import start_cli
            start_cli( self.mode )
        elif self.mode == EImRunMode.JUP:
            from intermake.commander.framework.console_parser import _PyiCommandWrapper
            builtins.__dict__.update( _PyiCommandWrapper.get_dict( self.app ) )  # TODO: This isn't great to override the builtins, how do we get out of it?
            return True  # Asynchronous
        elif self.mode == EImRunMode.PYS:
            return True  # Asynchronous
        elif self.mode == EImRunMode.PYI:
            import code
            from intermake.commander.framework.console_parser import _PyiCommandWrapper, INTERMAKE_PROMPT
            variables = _PyiCommandWrapper.get_dict( self.app )
            sys.ps1 = INTERMAKE_PROMPT
            shell = code.InteractiveConsole( variables )
            shell.interact( banner = "" )
        else:
            raise SwitchError( "self.mode", self.mode )
    
    
    def on_execute( self, xargs: Result ) -> None:
        """
        OVERRIDE
        """
        #
        # Clear the screen before a command if necessary
        #
        if console_configuration.current.clear_screen:
            io_helper.system_cls()
            echo = True
        elif console_configuration.current.force_echo:
            echo = True
        else:
            echo = False
        
        #
        # Echo the command name if necessary
        #
        if echo:
            self.__echo_command( xargs )
        
        #
        # Execute the actual command and issue the callbacks (we are single
        # threaded so there is no deferment here).
        #
        xargs.execute( SxsToStderrWriter( console_configuration.current ) )
        xargs.issue_callbacks()
    
    
    def __echo_command( self, xargs ) -> None:
        msg = ["COMMAND: {}".format( xargs.command.name )]
        
        for arg in xargs.command.args:
            value = arg.extract( *xargs.args.args, **xargs.args.kwargs )
            assert isinstance( arg, ArgInspector )
            
            n = xargs.command.name if xargs.command.name == arg.name else "{} {}".format( xargs.command.name, arg.name )
            msg.append('* {} = {}'.format( n, value ))
        
        print( "<echo>{}</echo>".format( "".join( msg ) ), file = sys.stderr )
