"""
`intermake.printing` or `intermake.pr` supplements output with HTML-like formatting "SXS-HTML"
and redirects output to the GUI if that is the active frontend. Please see the `sxshtml` library
for full formatting details. 
"""
from typing import Optional, Union, Sequence, Iterable, IO, TypeVar, Callable, Tuple
from mhelper import exception_helper, string_helper, ansi_helper, TracebackCollection

import re
import textwrap
import time


T = TypeVar( "T" )
TIterable = Union[Sequence[T], Iterable[T], IO]
TText = Optional[Union[str, Callable[[int], str]]]


class SxsWriter:
    """
    Utility class that provides functions for creating SXS XML.
    """
    __slots__ = ["__target"]
    
    
    def __init__( self, target: Callable[[str], None] ):
        """
        CONSTRUCTOR
        
        :param target:  Where to send formatted text. Typically the `write`
                        method of an `SxsToFormatter` object. 
        """
        self.__target = target
    
    
    def __call__( self, *args, **kwargs ):
        self.printx( *args, **kwargs )
    
    
    def printx( self, message, *args, end = "\n" ):
        """
        Sends the message through Intermake's message relay, allowing rich formatting of a message that is
        independent of the GUI.
        
        :param message: Message. May contain XML formatting as defined in `sxsxml`. 
        :param args:    Arguments. The message is formatted with these arguments. 
        :param end:     Line end character. 
        :return: 
        """
        assert isinstance(message, str), message
        
        if args:
            message = self.fmt( message, *args )
        
        self.__target( message + end )
    
    
    def fmt_rst( self, data: Optional[Union[str, bytes]] ) -> str:
        """
        Formats a block of RST text.
        """
        import docutils.core
        
        data = data or ""
        data = textwrap.dedent( data )
        data = docutils.core.publish_parts( source = data, writer_name = 'xml' )["whole"]
        data = data.split( '-->', 1 )[1]
        return data
    
    
    def pr_rst( self, data: Optional[Union[str, bytes]] ):
        """
        Formats and prints a block of RST text.
        """
        self.printx( self.fmt_rst( data ) )
    
    
    def pr_list( self, items ):
        """
        Prints a list of items.
        """
        self.printx( "<ul>{}</ul>".format( "".join( "<li>{}</li>".format( self.escape( x ) ) for x in items ) ) )
    
    
    def escape( self, text: str, condition: bool = True ) -> str:
        """
        Escapes HTML-codes in text, allowing the text to be rendered verbatim with `printx`.
        """
        text = str( text )
        
        if not condition:
            return text
        
        if "\033" in text:
            text = "(ANSI_STRIPPED:)" + ansi_helper.without_ansi( text )
        
        text = text.replace( "<", "&lt;" )
        text = text.replace( ">", "&gt;" )
        text = text.replace( '"', "&quot;" )
        text = text.replace( "'", "&apos;" )
        return text
    
    
    _escape = escape
    
    
    def fmt( self, message, *args ):
        """
        `builtins.format`s a `message` with all `args` passed through `escape`.
        """
        return message.format( *(self.escape( x ) for x in args) )
    
    
    def pr_information( self, message, *, end = "\n" ):
        """
        `escape`s the `message` and passes it to `printx`.
        """
        self.printx( self.escape( message ), end = end )
    
    
    def pr_style( self, style, message ):
        """
        Gives the message the designated style and passes it to `printx`.
        """
        self.printx( self.fmt_style( style, message ) )
    
    
    def fmt_style( self, style, message ):
        """
        Gives the message the designated style.
        """
        return "<{0}>{1}</{0}>".format( style, self.escape( message ) )
    
    
    def pr_question( self, message, options = () ):
        """
        Sends a `<question>` to printx. 
        """
        self.printx( self.fmt_question( message, options ) )
        return input()
    
    
    def fmt_question( self, message, options ):
        """
        Formats a `<question>`. 
        """
        return "<question>{}</question>".format( self.escape( message ), "".join( "<option>{}</option>".format( self.escape( x ) ) for x in options ) )
    
    
    def pr_table( self, rows, *, escape = True ):
        self.printx( self.fmt_table( rows, escape = escape ) )
    
    
    def fmt_table( self, rows, *, escape = True ):
        r = ["<table>"]
        for row in rows:
            r.append( self.fmt_row( row, escape = escape ) )
        r.append( "</table>" )
        return "\n".join( r )
    
    
    def fmt_row( self, items, *, escape = True ):
        return "<tr>{}</tr>".format( "".join( "<td>{}</td>".format( self._escape( item, escape ) ) for item in items ) )
    
    
    def fmt_file( self, file ):
        """
        Formats a `<file>` 
        """
        return "<file>{}</file>".format( self.escape( file ) )
    
    
    def pr_verbose( self, message: str ):
        """
        Sends a `<verbose>` to printx. 
        """
        self.printx( "<verbose>{}</verbose>".format( self.escape( message ) ) )
    
    
    def pr_warning( self, message: str, *_, **__ ):
        """
        Sends a `<warning>` to printx. 
        
        * and ** are ignored, these are for compatibility with warnings.showwarning
        """
        self.printx( "<warning>{}</warning>".format( self.escape( message ) ) )
    
    
    def fmt_code( self, message: str ):
        return "<code>{}</code>".format( self.escape( message ) )
    
    
    def fmt_section_start( self, name: str ):
        return '<section name="{}">'.format( self.escape( name ) )
    
    
    def fmt_section_end( self ):
        return '</section>'
    
    
    def pr_enumerate( self, *args, **kwargs ) -> Iterable[Tuple[int, T]]:
        """
        Equivalent to `enumerate(pr_iterate(...))`.
        """
        return enumerate( self.pr_iterate( *args, **kwargs ) )
    
    
    def pr_iterate( self, iterable: TIterable, title: str, count: int = None, text: TText = None ) -> Iterable[T]:
        """
        Iterates over an iterable and relays the progress to the GUI as SXS-HTML.
        
        :param iterable:        What to iterate over. This can be any iterable, though special cases for `list` `tuple` and file objects allow an automated `count`. 
        :param title:           See `pr.action.__init__` 
        :param count:           See `pr.action.__init__`.
                                The default is len(iterable), or the length of the file in bytes (for a file object). 
        :param text:            See `pr.action.__init__`.
        :return:                Yields each item from `iterable`. 
        """
        if count is None:
            try:
                count = len( iterable )
                count_is_file = False
            except TypeError:
                try:
                    original_position = iterable.tell()
                    count = iterable.seek( 0, 2 )
                    iterable.seek( original_position )
                    count_is_file = True
                except AttributeError:
                    count = 0
                    count_is_file = False
        else:
            count_is_file = False
        
        with self.pr_action( self, title, count, text ) as action_:
            for x in iterable:
                if count_is_file:
                    action_.set_value( iterable.tell() )
                else:
                    action_.increment()
                
                yield x
    
    
    def pr_traceback( self, exception: Exception ):
        """
        Formats an exception traceback as SXS-HTML and sends it to `printx`.
        """
        self.printx( self.fmt_traceback( exception ) )
    
    
    def fmt_traceback( self, exception: Union[BaseException, str], traceback: TracebackCollection = None ) -> str:
        """
        Formats an error traceback as SXS-HTML.
        
        :param exception:       Exception to display 
        :param traceback:      Traceback text (leave as `None` to get the system traceback) 
        :return:                ANSI containing string  
        """
        r = []
        r.append( "<section name='Traceback'>" )
        
        if not traceback:
            traceback: TracebackCollection = exception_helper.get_traceback_ex( exception )
        
        for exx in traceback.exceptions:
            r.append( self.fmt( "<section name='{}'>", exx.type ) )
            
            for frr in exx.frames:
                r.append( self.fmt( "<section name='Frame {}'>", frr.index ) )
                r.append( self.fmt( "<file>{}</file> <key>{}</key> <key>{}</key>", frr.file_name, frr.line_no, frr.function ) )
                r.append( self.fmt( frr.code_context.replace( frr.next_function, "<key>" + frr.next_function + "</key>" ) ) )
                
                r.append( self.fmt( "<section name='{} locals'>", len(frr.locals) ) )
                for local in frr.locals:
                    r.append( self.fmt( "<key>{}</key> = <value>{}</value>", local.name, string_helper.max_width( str( local.value ), 40 ) ) )
                r.append( self.fmt( "</section>" ) )
                
                r.append( self.fmt( "</section>" ) )
            
            r.append( self.escape( exx.type ) )
            r.append( self.escape( str( exx.exception ) ) )
            
            r.append( "</section>" )
        
        # Exception text
        cause_depth = 1
        ex = exception
        prev = None
        
        while ex:
            if prev is not None:
                r.append( "<section name='Exception: {} - cause of {}'>".format( type( ex ).__name__, type( prev ).__name__ ) )
            
            else:
                r.append( "<section name='Exception: {}'>".format( type( ex ).__name__ ) )
            
            cause_depth += 1
            txt = self.escape( str( ex ) )
            txt = string_helper.highlight_quotes( txt, "«", "»", "<code>", "</code>" )
            r.append( txt )
            
            if isinstance( exception, BaseException ):
                prev = ex
                ex = ex.__cause__
            else:
                ex = None
        
        for _ in range( cause_depth ):
            r.append( "</section>" )
        
        return "\n".join( r )
    
    
    def pr_section( self, *args, **kwargs ) -> "_ManagedSection":
        return _ManagedSection( self, *args, **kwargs )
    
    
    def pr_action( self, *args, **kwargs ) -> "_ManagedAction":
        return _ManagedAction( self, *args, **kwargs )


class _ManagedSection:
    """
    An enter...exit block that sends a `<section>` and `</section>` to `printx`.
    """
    __slots__ = ["__owner"]
    
    
    def __init__( self, owner, name ):
        self.__owner = owner
        owner.printx( owner.fmt_section_start( name ) )
    
    
    def __enter__( self ):
        return self
    
    
    def __exit__( self, exc_type, exc_val, exc_tb ):
        self.__owner.printx( self.__owner.fmt_section_end() )


class _ManagedAction:
    """
    An enter...exit block that sends an `<action>` and `</action>` to `printx`
    with support for periodic progress updates via `<progress>`.
    """
    __slots__ = ["__owner", "__value", "__next_issue", "__get_text"]
    
    
    def __init__( self, owner, title: str, count: int = 0, text: TText = None ):
        self.__owner = owner
        owner.printx( '<action name="{}" max="{}">'.format( owner.escape( title ), count ) )
        self.__value = 0
        self.__next_issue = 0
        
        if text is None:
            self.__get_text = lambda x: ""
        elif isinstance( text, str ):
            closure = text
            self.__get_text = lambda x: closure.format( x )
        else:
            self.__get_text = text
        
        self.still_alive()
    
    
    def __enter__( self ):
        return self
    
    
    def __exit__( self, exc_type, exc_val, exc_tb ):
        self.__owner.printx( '</action>' )
    
    
    def increment( self, n = 1 ):
        self.__value += n
        self.still_alive()
    
    
    def set_value( self, value ):
        self.__value = value
        self.still_alive()
    
    
    def still_alive( self ):
        if time.time() > self.__next_issue:
            text = self.__get_text( self.__value )
            self.__owner.printx( '<progress value="{}">{}</progress>'.format( self.__value, self.__owner.escape( text ) ) )
            self.__next_issue = time.time() + 0.2
